# What fraction of cellular DNA turnover becomes cfDNA?


This repository is accompanying the paper ["What fraction of cellular DNA turnover becomes cfDNA?"]()

Ron Sender, Elad Noor, Ron Milo*, Yuval Dor*

*corresponding aothurs: Ron Milo, ron.milo@weizmann.ac.il; Yuval Dor yuvald@ekmd.huji.ac.il


This repository contain a jupyter notebook file and an xlsx file.
All data is given in **[`data.xlsx`](./data.xlsx)|** 

The notebooks **[`cfDNA_and_turnover.ipynb`](cfDNA_and_tunrover.ipynb)|** contains code calculating the ratio between the potential DNA flux from cellular turnover and the cfDNA levels found in the blood. The notebook details also the uncertainty estimate as well as creating Figure 1.
